from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

import nltk
nltk.download('punkt')
from nltk.translate.bleu_score import sentence_bleu
from nltk.tokenize import word_tokenize

from rouge import Rouge


def eval_answer_cos(model_ans, gt_ans):
    """
    calculate the cosine similarity
    model_ans: the prediction of model
    gt_ans: the groundtruth answer
    """
    # initialize the TF-IDF vectorizer
    vectorizer = TfidfVectorizer()
    
    # combine both model prediction and groudtruth answer
    documents = [model_ans, gt_ans]
    
    # cal tf-idf
    tfidf_mat = vectorizer.fit_transform(documents)
    
    # cal cos similarity
    cosine_sim = cosine_similarity(tfidf_mat[0:1], tfidf_mat[1:2])
    
    score = cosine_sim[0][0]
    
    return score

def eval_answer_bleu(model_ans, gt_ans):
    """
    calculate the bleu score of two sentences
    model_ans: the prediction of model
    gt_ans: the groundtruth answer
    """
    # tokenize the gt_ans text using NLTK
    gt_tokens = word_tokenize(gt_ans)
    pred_tokens = word_tokenize(model_ans)
    
    bleu = sentence_bleu([gt_tokens], pred_tokens)

    return bleu

def eval_answer_rouge(model_ans, gt_ans):
    """
    calculate the ROUGE score of two sentences
    model_ans: the prediction of model
    gt_ans: the groundtruth answer
    """
    model_ans = ' '.join(model_ans.split())
    gt_ans = ' '.join(gt_ans.split())
    # initialize the rouge object
    rouge = Rouge()
    import ipdb; ipdb.set_trace()
    rouge_score = rouge.get_scores(model_ans, gt_ans)
    # pass
    rouge1_f1 = rouge_score['f']
    return rouge_score

def edit_score(s1, s2):
    """
    Compute the normalized edit score between two strings s1 and s2.

    Args:
    s1 (str): First string.
    s2 (str): Second string.

    Returns:
    float: Normalized edit score between s1 and s2.
    """
    m, n = len(s1), len(s2)
    # Initialize the distance matrix
    dp = [[0] * (n + 1) for _ in range(m + 1)]

    # Base cases
    for i in range(m + 1):
        dp[i][0] = i
    for j in range(n + 1):
        dp[0][j] = j

    # Compute edit distance
    for i in range(1, m + 1):
        for j in range(1, n + 1):
            if s1[i - 1] == s2[j - 1]:
                dp[i][j] = dp[i - 1][j - 1]
            else:
                dp[i][j] = 1 + min(dp[i - 1][j],    # Deletion
                                   dp[i][j - 1],    # Insertion
                                   dp[i - 1][j - 1]  # Replacement
                                  )

    # Normalized edit distance
    try:
        normalized_distance = dp[m][n] / max(m, n)
    except:
        # import ipdb; ipdb.set_trace()
        normalized_distance = 0
    return 1-normalized_distance

if __name__ == '__main__':
    
    model_ans = "python"
    gt_ans = "python"
    # cos_score = eval_answer_cos(model_ans, gt_ans)
    # bleu_score = eval_answer_bleu(model_ans, gt_ans)
    # rouge_score = eval_answer_rouge(model_ans, gt_ans)
    import ipdb; ipdb.set_trace()
    edit_distance_score = edit_score(model_ans, gt_ans)
    print(edit_distance_score)
    