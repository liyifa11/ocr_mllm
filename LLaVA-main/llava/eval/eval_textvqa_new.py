import json
import os
import argparse
import re
from tqdm import tqdm
import shortuuid
import torch

# import sys
# sys.path.append('/home/liyifa11/MyCodes/LLM891/ocr_mllm/LLaVA-main/')
from llava.eval.m4c_evaluator import TextVQAAccuracyEvaluator, STVQAEditScoreEvaluator
from llava.constants import IMAGE_TOKEN_INDEX, DEFAULT_IMAGE_TOKEN, DEFAULT_IM_START_TOKEN, DEFAULT_IM_END_TOKEN
from llava.conversation import conv_templates, SeparatorStyle
from llava.model.builder import load_pretrained_model
from llava.utils import disable_torch_init
from llava.mm_utils import tokenizer_image_token, process_images, get_model_name_from_path

from PIL import Image
import math


def eval_model(args):
    # Model
    disable_torch_init()
    model_path = os.path.expanduser(args.model_path)
    model_name = get_model_name_from_path(model_path)
    # import ipdb; ipdb.set_trace()
    tokenizer, model, image_processor, context_len = load_pretrained_model(model_path, args.model_base, model_name)
    with open(args.eval_file) as f:
        eval_annos = json.load(f)
    answers_file = os.path.join(args.answers_dir, f"{model_name}/answer.jsonl")
    answers_file = os.path.expanduser(answers_file)
    
    if os.path.exists(answers_file):
        pred_list, ans_list = [], []
        with open(answers_file, 'r', encoding='utf-8') as file:
            for line in file:
                json_obj = json.loads(line)
                pred_list.append(json_obj['text'])
                ans_list.append(json_obj['gt'])
    else:
        os.makedirs(os.path.dirname(answers_file), exist_ok=True)
        ans_file = open(answers_file, "w")
        
        pred_list, ans_list = [], []
        
        for idx, anno in tqdm(enumerate(eval_annos)):
            
            image_file = anno["image"]
            qs = anno["conversations"][0]["value"]
            ans = anno["conversations"][1]["value"]
            ans_list.append(ans)

            cur_prompt = qs
            if model.config.mm_use_im_start_end:
                qs = DEFAULT_IM_START_TOKEN + DEFAULT_IMAGE_TOKEN + DEFAULT_IM_END_TOKEN + '\n' + qs
            else:
                qs = qs

            conv = conv_templates[args.conv_mode].copy()
            conv.append_message(conv.roles[0], qs)
            conv.append_message(conv.roles[1], None)
            prompt = conv.get_prompt()

            input_ids = tokenizer_image_token(prompt, tokenizer, IMAGE_TOKEN_INDEX, return_tensors='pt').unsqueeze(0).cuda()

            image = Image.open(os.path.join(args.image_folder, image_file)).convert('RGB')
            image_tensor = process_images([image], image_processor, model.config)[0]

            with torch.inference_mode():
                output_ids = model.generate(
                    input_ids,
                    images=image_tensor.unsqueeze(0).half().cuda(),
                    image_sizes=[image.size],
                    do_sample=True if args.temperature > 0 else False,
                    temperature=args.temperature,
                    top_p=args.top_p,
                    num_beams=args.num_beams,
                    # no_repeat_ngram_size=3,
                    max_new_tokens=1024,
                    use_cache=True)

            outputs = tokenizer.batch_decode(output_ids, skip_special_tokens=True)[0].strip()
            pred_list.append(outputs)
            ans_id = shortuuid.uuid()
            ans_file.write(json.dumps({"question_id": idx,
                                    "prompt": cur_prompt,
                                    "text": outputs,
                                    "gt": ans,
                                    "answer_id": ans_id,
                                    "model_id": model_name,
                                    "metadata": {}}) + "\n")
            ans_file.flush()
        ans_file.close()
    return pred_list, ans_list


def prompt_processor(prompt):
    if prompt.startswith('OCR tokens: '):
        pattern = r"Question: (.*?) Short answer:"
        match = re.search(pattern, prompt, re.DOTALL)
        question = match.group(1)
    elif 'Reference OCR token: ' in prompt and len(prompt.split('\n')) == 3:
        if prompt.startswith('Reference OCR token:'):
            question = prompt.split('\n')[1]
        else:
            question = prompt.split('\n')[0]
    elif len(prompt.split('\n')) == 2:
        question = prompt.split('\n')[0]
    else:
        assert False

    return question.lower()


def eval_single(pred_list, ans_list):
    pred_ans_list = []
    for pred, ans in zip(pred_list, ans_list):
        pred_ans_list.append(
            {
                "pred_answer": pred,
                "gt_answers": [ans]
            }
        )
    # import ipdb; ipdb.set_trace()

    evaluator = TextVQAAccuracyEvaluator()
    edit_evaluator = STVQAEditScoreEvaluator()
    acc = evaluator.eval_pred_list(pred_ans_list)
    edit_score = edit_evaluator.eval_pred_list(pred_ans_list)
    print(f'Samples: {len(pred_ans_list)}, Accuracy: {acc*100:.2f}, Edit score: {edit_score*100:.2f}')


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--model-path", type=str, default="/home/liyifa11/MyCodes/LLM891/ocr_mllm/LLaVA-main/checkpoints/llava-v1.5-7b-text_ocr_spdoc-lora")
    # parser.add_argument("--model-path", type=str, default="/home/liyifa11/MyCodes/LLM891/ocr_mllm/LLaVA-main/checkpoints/llava-v1.5-7b")
    parser.add_argument("--model-base", type=str, default="/home/liyifa11/MyCodes/LLM891/ocr_mllm/LLaVA-main/checkpoints/llava-v1.5-7b")
    # parser.add_argument("--model-base", type=str, default=None)
    parser.add_argument("--image-folder", type=str, default="/home/liyifa11/MyCodes/LLM891/ocr_mllm/LLaVA-main/data")
    parser.add_argument("--answers-dir", type=str, default="/home/liyifa11/MyCodes/LLM891/ocr_mllm/LLaVA-main/output/")
    parser.add_argument("--eval-file", type=str, default="/home/liyifa11/MyCodes/LLM891/ocr_mllm/LLaVA-main/data/annotations/TextVQA_0.5.1_val_updated.json")
    parser.add_argument("--conv-mode", type=str, default="llava_v1")
    parser.add_argument("--temperature", type=float, default=0.2)
    parser.add_argument("--top_p", type=float, default=None)
    parser.add_argument("--num_beams", type=int, default=1)
    args = parser.parse_args()
    # import ipdb; ipdb.set_trace()
    pred_list, ans_list = eval_model(args)
    eval_single(pred_list, ans_list)