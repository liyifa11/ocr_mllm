import torch
import torch.nn as nn
import re


class IdentityMap(nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, x, *args, **kwargs):
        return x

    @property
    def config(self):
        return {"mm_projector_type": 'identity'}


class SimpleResBlock(nn.Module):
    def __init__(self, channels):
        super().__init__()
        self.pre_norm = nn.LayerNorm(channels)

        self.proj = nn.Sequential(
            nn.Linear(channels, channels),
            nn.GELU(),
            nn.Linear(channels, channels)
        )
    def forward(self, x):
        x = self.pre_norm(x)
        return x + self.proj(x)


def build_vision_projector(config, delay_load=False, **kwargs):
    projector_type = getattr(config, 'mm_projector_type', 'linear')

    if projector_type == 'linear':
        return nn.Linear(config.mm_hidden_size, config.hidden_size)
    
    if 'moe' in projector_type:
        return DenseMoE(3, config.mm_hidden_size, config.hidden_size)
        
    mlp_gelu_match = re.match(r'^mlp(\d+)x_gelu$', projector_type)
    if mlp_gelu_match:
        mlp_depth = int(mlp_gelu_match.group(1))
        modules = [nn.Linear(config.mm_hidden_size, config.hidden_size)]
        for _ in range(1, mlp_depth):
            modules.append(nn.GELU())
            modules.append(nn.Linear(config.hidden_size, config.hidden_size))
        return nn.Sequential(*modules)

    if projector_type == 'identity':
        return IdentityMap()

    raise ValueError(f'Unknown projector type: {projector_type}')


class MLP(nn.Module):
    def __init__(self, in_channels, hidden_dim, out_channels):
        super(MLP, self).__init__()
        self.mlp = nn.Sequential(
            nn.Linear(in_channels, hidden_dim),
            nn.GELU(),
            nn.Linear(hidden_dim, out_channels)
        )
        
    def forward(self, x):
        return self.mlp(x)


class DenseMoE(nn.Module):
    def __init__(self, num_experts, in_channels, out_channels):
        super(DenseMoE, self).__init__()
        self.experts = nn.ModuleList([MLP(in_channels, out_channels, out_channels) for i in range(num_experts)])
        self.gates = MLP(in_channels, out_channels, num_experts)
    
    def forward(self, x):
        """
        x: visual_tokens (B, L, D)
        """
        gate_weights = self.gates(x) # (B, L, N_expert)
        expert_outs = []
        for i, expert in enumerate(self.experts):
            weight_i = gate_weights[:, :, (i,)] # (B, L, 1)
            out = expert(x * weight_i) # (B, L, D)
            expert_outs.append(out)
        aggr_out = sum(expert_outs)
        return aggr_out